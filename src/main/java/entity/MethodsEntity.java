package entity;

import annotation.Secured;

public class MethodsEntity {

    @Secured(value = 1)
    public void frstMethod(){
    }

    @Secured(value = 2, name = "secmeth")
    private void scndMethod(){
    }

    public void thrdMethod(){
    }
}