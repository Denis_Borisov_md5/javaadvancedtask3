import annotation.Secured;
import entity.MethodsEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        Method[] methods = MethodsEntity.class.getDeclaredMethods();

        for(Method method : methods){
            Secured isSecured = method.getAnnotation(Secured.class);
            logger.info(method + "/" + isSecured);
        }
    }
}
